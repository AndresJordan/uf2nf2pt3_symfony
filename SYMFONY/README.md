# Imagen

![LOCAL](imagen001.png)

![LABS](imagen002.PNG)

# Manuales

[Manual 1](https://symfony.com/doc/3.3/setup.html)

[Manual 2](http://symfony.com/doc/current/setup.html)

[Manual 3](http://symfony.com/doc/current/setup/built_in_web_server.html)

[Manual 4](https://symfony.com/doc/2.6/book/installation.html)

[Manual 5](https://www.digitalocean.com/community/tutorials/how-to-deploy-a-symfony-application-to-production-on-ubuntu-14-04)

# Errores

## Local ##

```
Failed to download symfony/symfony-demo from dist: The zip extension and unzip command are both missing, skipping.
Your command-line PHP is using multiple ini files. Run `php --ini` to show them.
    Now trying to download from source
```

Soluci�n: apt install unzip && apt install zip

```
[ErrorException]            
  mkdir(): Permission denied  
```

Soluci�n: asignar permisos

# Diferencia local y labs

Por temas de permisos en local se puede mostrar la web y en el labs no.



